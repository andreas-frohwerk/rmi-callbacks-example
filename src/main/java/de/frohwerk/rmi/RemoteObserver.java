package de.frohwerk.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteObserver extends Remote {

    void accept(final String message) throws RemoteException;

}
