package de.frohwerk.rmi;

import de.frohwerk.rmi.RemoteObserver;

import java.util.function.Consumer;

public class RemoteObserverAdapter implements RemoteObserver {
    private transient final Consumer<String> adaptee;

    public RemoteObserverAdapter(final Consumer<String> adaptee) {
        this.adaptee = adaptee;
    }

    @Override
    public void accept(final String message) {
        adaptee.accept(message);
    }
}
