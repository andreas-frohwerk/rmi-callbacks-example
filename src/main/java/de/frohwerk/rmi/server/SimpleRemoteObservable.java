package de.frohwerk.rmi.server;

import de.frohwerk.rmi.RemoteObservable;
import de.frohwerk.rmi.RemoteObserver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class SimpleRemoteObservable implements RemoteObservable {
    private static final Logger log = LogManager.getLogger(SimpleRemoteObservable.class);

    private final List<RemoteObserver> observers = Collections.synchronizedList(new ArrayList<>());

    @Override
    public void send(final String message) {
        observers.forEach(otherObserver -> {
            try {
                otherObserver.accept(message);
            } catch (final RemoteException ex) {
                log.warn("Error notifying observer: {}", ex.getMessage());
            }
        });
    }

    @Override
    public void subscribe(final RemoteObserver observer) {
        log.info("Adding observer...");
        observers.forEach(otherObserver -> {
            try {
                otherObserver.accept("An observer joined");
            } catch (final RemoteException ex) {
                log.warn("Error notifying observer: {}", ex.getMessage());
            }
        });
        observers.add(observer);
    }
}
