package de.frohwerk.rmi.server;

import de.frohwerk.cli.CommandLineInterface;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServerApplication {
    public static void main(String[] args) throws Exception {
        final SimpleRemoteObservable simpleRemoteObservable = new SimpleRemoteObservable();

        final Registry registry = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
        registry.rebind("RemoteObservable", UnicastRemoteObject.exportObject(simpleRemoteObservable, 0));

        CommandLineInterface.start(command -> System.out.printf("Unknown command: %s\n", command));

        registry.unbind("RemoteObservable");
        UnicastRemoteObject.unexportObject(simpleRemoteObservable, true);
    }
}
