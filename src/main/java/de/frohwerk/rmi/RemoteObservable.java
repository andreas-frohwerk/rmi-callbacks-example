package de.frohwerk.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteObservable extends Remote {

    void send(final String message) throws RemoteException;

    void subscribe(final RemoteObserver observer) throws RemoteException;

}
