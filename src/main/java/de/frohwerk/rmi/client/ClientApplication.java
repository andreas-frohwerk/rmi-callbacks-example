package de.frohwerk.rmi.client;

import de.frohwerk.cli.CommandLineInterface;
import de.frohwerk.rmi.RemoteObservable;
import de.frohwerk.rmi.RemoteObserver;
import de.frohwerk.rmi.RemoteObserverAdapter;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ClientApplication {
    public static void main(final String[] args) throws Exception {
        final RemoteObserverAdapter remoteObserverAdapter = new RemoteObserverAdapter(s -> System.out.printf("%s\n> ", s));

        final Registry registry = LocateRegistry.getRegistry(Registry.REGISTRY_PORT);
        final RemoteObservable remoteObservable = (RemoteObservable) registry.lookup("RemoteObservable");

        remoteObservable.subscribe((RemoteObserver) UnicastRemoteObject.exportObject(remoteObserverAdapter, 0));

        CommandLineInterface.start("Enter '/send ...' to send a message or 'exit' to exit", command -> {
            if (command.startsWith("/send ") && command.length() > 6) {
                try {
                    remoteObservable.send(command.substring(6));
                } catch (final RemoteException ex) {
                    ex.printStackTrace();
                }
            } else {
                System.out.println("Unknown command: " + command);
            }
        });

        UnicastRemoteObject.unexportObject(remoteObserverAdapter, true);
    }
}
