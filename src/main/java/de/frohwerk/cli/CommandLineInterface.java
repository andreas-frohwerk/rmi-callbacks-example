package de.frohwerk.cli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.Consumer;

public class CommandLineInterface {
    private CommandLineInterface() { /* Utility class - No instantiation allowed */ }

    /**
     * Run a loop on the standard input, pass each input (line) to the supplied handler with the exception of the 'exit'
     * command. The command 'exit' terminates the input loop instead.
     *
     * @param commandHandler Callback that allows to handle application specific commands
     * @throws IOException if an io error occurs during the input loop
     */
    public static void start(final Consumer<String> commandHandler) throws IOException {
        start("Enter 'exit' (without the quotes) to shutdown the server", commandHandler);
    }

    /**
     * Run a loop on the standard input, pass each input (line) to the supplied handler with the exception of the 'exit'
     * command. The command 'exit' terminates the input loop instead. Displays the specified greeting message on startup
     * to instruct the user about available commands
     *
     * @param greeting       Message to display on startup to instruct the user about available commands
     * @param commandHandler Callback that allows to handle application specific commands
     * @throws IOException if an io error occurs during the input loop
     */
    public static void start(final String greeting, final Consumer<String> commandHandler) throws IOException {
        System.out.println(greeting);
        System.out.print("> ");
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            for (String command = reader.readLine(); command != null; command = reader.readLine()) {
                System.out.print("> ");
                if ("exit".equalsIgnoreCase(command)) {
                    return;
                } else {
                    commandHandler.accept(command);
                }
            }
        }
    }
}
