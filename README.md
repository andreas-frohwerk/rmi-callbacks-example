# Example: How to use callbacks with RMI

This example project contains two applications (ClientApplication and ServerApplication), two Remote interfaces
(RemoteObservable and RemoteObserver) and a basic implementation of both interfaces.

The ServerApplication starts a rmi registry on the default port (1099) and therefore cannot run multiple instances. The
ClientApplication on the other hand can (and for demonstration purposes should) be started multiple times.

In a real-world scenario the base package (rmi) would probably be used as a shared library for both the client and
server application (which would most likely not be part of the same jar).

Both client and server feature a very basic shell. Both support the command 'exit' to terminate the application. The
client also supports a '/send ...' command to send a text message to all observers.

Since this is meant to be a basic example there is no handling of disconnects.

## Explanation: exportObject vs. Serializable

Using objects with RMI requires the objects to either be serializable (transient fields are allowed) or exposure of the
objects on the network. This can be achieved via inheritance from UnicastRemoteObject or using the static method:
```UnicastRemoteObject.exportObject(Remote remote, int port)```. Specifying port 0 let's the runtime choose a random
available port.

Exported objects are shared across JVMs and machines, modifying one instance should also affect the others. Remote VMs
or machines use a stub, the actual modification is done remotely on the original object. Serializable objects on the
other hand are copied, the most basic example being a string passed to a remote object. Further changes to unexported 
serializable objects are not propagated to those copies.

## Further notes

RMI callbacks require the clients to expose RMI objects over the network. Basically it turns clients into servers. This
is probably not possible on corporate networks or other restricted environments. There are technologies better suited
for those use cases (e.g. WebSocket), this example project was created for academic purposes.